# syntax=docker/dockerfile:1
FROM python:3.12
LABEL authors="maxsergeev39@gmail.com"

WORKDIR /tmp/docker/

COPY requirements.txt requirements.txt
RUN python -m pip install -r requirements.txt

COPY src .

CMD [ "python", "-m", "flask", "run", "--host=0.0.0.0" ]